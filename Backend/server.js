var express = require('express');
const database = require('./Database/mongodb');
const api = require('./request/api');

var productsRouter = require('./routes/products');

var app = express();
database.connectDB();
app.use(express.json());

app.use('/products', productsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  res.send(404);
})

const loadArticles= ()=>{
  api.articles(async function(req, res, next) {
    let articles = res.body;
    articles.map((article)=>{
      let saveItem = new database.articleModel(article);
      database.insertDocument(saveItem);
    })
  })
  console.log("Load Articles")
}
loadArticles()
setInterval(()=>{
  loadArticles()
}, 1000 *60*60)

app.listen(3000, () => {
  console.log(`App listening on port ${3000}`);
})
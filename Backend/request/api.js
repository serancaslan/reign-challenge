const request = require('postman-request');

const articles = (callback) => {
  const url = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs'
  request.get({ url, json: true }, (error, { body }) => {
    if (error) {
      callback('Unable to connect to location services', undefined)
    } else {
      callback(undefined, {
        body: body.hits
      })
    }
  })
}

module.exports = {
  articles
}
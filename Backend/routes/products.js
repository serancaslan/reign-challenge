const express = require('express');
const router = express.Router();
const database = require('../Database/mongodb');

/* POST Delete article for user. */
router.post('/delete', async function(req, res, next) {
  let page = req.query.page;
  let filter = req.query.filter
  let item = req.query.item
  let data = await database.getPage(database.articleModel, page, filter)
  if(parseInt(item)){
    if(data[parseInt(item)-1]){
      let { objectID } = data[parseInt(item-1)]
      let articleId = new database.blacklistModel({objectID})
      database.insertDocument(articleId)
      res.send({data: "article in blacklist"}).status(200)
    }else{
      res.send({Message: "Article do not exsist"}).status(200)
    }
  }else{
    res.send({Message: "Item is not a number"}).status(200)
  }
});

/* GET users listing. */
router.get('/', async function(req, res, next) {
  let page = req.query.page;
  let filter = req.query.filter
  let data = await database.getPage(database.articleModel, page, filter);
  res.send({data}).status(200)
});

module.exports = router;

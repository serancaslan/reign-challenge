const mongoose = require('mongoose');

const articleSchema = new mongoose.Schema({
  created_at:  Date,
  title: String,
  url: String,
  author: String,
  points: String,
  story_text: String,
  comment_text: String,
  num_comments: Number,
  story_id: Number,
  story_title: String,
  story_url: String,
  parent_id: Number,
  created_at_i: Number,
  _tags: Array,
  objectID: {
    type: String,
    index: true,
    unique: true 
  },
  _highlightResult: JSON
});

const blacklistSchema = new mongoose.Schema({
  objectID: String,
});

const articleModel = mongoose.model('article', articleSchema);
const blacklistModel = mongoose.model('blacklist', blacklistSchema);


// DB Methods ======================
const connectDB = async () => {
  await mongoose.connect(
    'mongodb://172.19.0.2:27019/reign', {
      authSource: "admin",
      user: "sergio97",
      pass: "Dadadada_97"
    }
  )
}


const insertDocument = (model) => {
  model.save(function(err, user) {
    if (err) {
        console.log("Article duplicated");
    }}
  )
}

const getDocument = async (model, name) => {
  return await model.find({name})
}

const getPage = async (model, page= 1, filter = 'default') => {
  console.log(filter);
  if(filter == 'default'){condition = {}
  }else{
    condition = JSON.parse(filter)
  }
  let blacklist= await blacklistModel.find({}).select('objectID -_id').exec()
  let blacklistArray = blacklist.map((e)=>{return e.objectID})
  if(blacklistArray.length>0){
    condition.objectID= { $nin: blacklistArray }
  }
  return await model.find(condition).skip(5*(page-1)).limit(5).exec()
}

const getCollection = async (model) => {
  return await model.find()
}

module.exports = {
  connectDB,
  insertDocument,
  getDocument,
  getPage,
  getCollection,
  articleModel,
  blacklistModel
}
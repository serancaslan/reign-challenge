# Reign-Challenge

## How to test the challenge

1. To run the app you need to have docker and npm intalled.
2. Go to /Backend.
3. Execute **npm install**
4. Execute **docker compose up --build**
5. Now you can run the GET and POST requests from the postman file.
6. Also if you want to see the database in MongoDB Compas, connect with the URL "mongodb://sergio97:Dadadada_97@localhost:27019".
